package com.pawelbanasik.domain;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.pawelbanasik.service.RecruitmentService;

@Component("myhrdept")
public class HumanResourceDept implements Department {

	private String deptName;

	@Autowired
	@Qualifier("agency")
	private RecruitmentService recruitmentService;

	@Autowired
	private Organization organization;

	public HumanResourceDept(RecruitmentService recruitmentService, Organization organization) {
		this.recruitmentService = recruitmentService;
		this.organization = organization;
	}

	public HumanResourceDept() {
	}

	public String hiringStatus(int numberOfRecruitments) {
		String hiringFacts = recruitmentService.recruitEmployees(organization.getCompanyName(), deptName,
				numberOfRecruitments);
		return hiringFacts;
	}

	@Autowired
	public void setDeptName(@Value("${deptName}") String deptName) {
		this.deptName = deptName;
	}

}
