package com.pawelbanasik.domain;

public interface Department {

	public String hiringStatus (int numberOfRecruitments);
}
 